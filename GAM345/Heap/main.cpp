#include <iostream>
#include <vector>
#include <stdexcept>
#include <string>
using namespace std;

template<typename T>
class MinHeap
{
public:

	T& Peek() const
	{
		if (data.size() == 0)
			throw logic_error("Heap::Peek() on empty heap!");

		return data[0];
	}

	void Insert(const T& value)
	{
		data.push_back(value);
		BubbleUp();
	}

	T Remove()
	{
		if (data.size() == 0)
			throw logic_error("Heap::Remove() on empty heap!");

		T result = data[0];

		// Replace root with the last element
		data[0] = data[data.size()-1];
		// Actually remove it.
		data.erase(data.begin() + (data.size() - 1));

		// Find where the new root actually belongs
		BubbleDown();

		return result;
	}

	int Size() const
	{
		return data.size();
	}

private:

	void BubbleUp()
	{
		int i = data.size() - 1;

		for (int parent = (i - 1) / 2; parent >= 0; parent = (i-1) / 2)
		{
			if (data[i] < data[parent] )
			{
				std::swap(data[parent], data[i]);
				i = parent;
			}
			else
				break;
		}

	}

	void BubbleDown()
	{
		int i = 0;

		while(true)
		{
			int left = 2 * i + 1;
			int right = 2 * i + 2;

			int childToSwapWith = i;

			if (left < data.size() && data[left] < data[i])
				childToSwapWith = left; // WE SHOULD SWAP WITH LEFT
			if (right < data.size() && data[right] < data[childToSwapWith])
				childToSwapWith = right; // USE RIGHT INSTEAD

			if (i == childToSwapWith)
				break;

			swap(data[i], data[childToSwapWith]);
			i = childToSwapWith;
		}
	}

	vector<T> data;
};

/*class Thing
{
public:
	int value;

	Thing(int a)
	{
		value = a;
	}

	bool operator<(const Thing& other)
	{
		return value < other.value;
	}

};*/

class Thing
{
public:
	int priority;
	string value;

	Thing(int newPriority, string newValue)
	{
		priority = newPriority;
		value = newValue;
	}

	bool operator<(const Thing& other)
	{
		return priority < other.priority;
	}

};


int main(int argc, char* argv[])
{
	MinHeap<int> myHeap;

	myHeap.Insert(5);
	myHeap.Insert(4);
	myHeap.Insert(7);
	myHeap.Insert(18);
	myHeap.Insert(1);
	myHeap.Insert(-34);
	myHeap.Insert(7);

	while (myHeap.Size() > 0)
	{
		cout << myHeap.Remove() << endl;
	}

	MinHeap<string> stringHeap;

	stringHeap.Insert("5");
	stringHeap.Insert("4");
	stringHeap.Insert("7");
	stringHeap.Insert("18");
	stringHeap.Insert("1");
	stringHeap.Insert("TONY");
	stringHeap.Insert("-34");
	stringHeap.Insert("7");

	while (stringHeap.Size() > 0)
	{
		cout << stringHeap.Remove() << endl;
	}

	MinHeap<Thing> thingHeap;

	thingHeap.Insert(Thing(5, "Jeff"));
	thingHeap.Insert(Thing(4, "Not Tony"));
	thingHeap.Insert(Thing(-25, "Putin"));
	thingHeap.Insert(Thing(18, "Son of Tony"));
	thingHeap.Insert(Thing(1, "Tony2EB"));
	thingHeap.Insert(Thing(1, "TONY"));
	thingHeap.Insert(Thing(-34, "Tony, Second Father of Tony"));
	thingHeap.Insert(Thing(7, "Random Jim"));

	while (thingHeap.Size() > 0)
	{
		cout << thingHeap.Remove().value << endl;
	}

	cin.get();

	return 0;
}