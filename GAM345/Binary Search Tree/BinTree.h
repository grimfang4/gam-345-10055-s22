#pragma once


class BinTree
{
public:

	BinTree();
	//~BinTree();  // Implies need for Rule of Three

	void Insert(int value);
	bool Contains(int value);
	void Erase(int value);
	int Size();
	void Print();  // Implement an "in-order" traversal: left, parent, right
	void Clear();

private:
	struct Node
	{
		int value;
		Node* left;
		Node* right;

		/*Node()
		{
			left = nullptr;
			right = nullptr;
			value = 0;
		}*/
		Node()
			: value(0), left(nullptr), right(nullptr)
		{}
	};

	Node* root;
	int size;
};
