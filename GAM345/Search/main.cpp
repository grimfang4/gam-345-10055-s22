#include <iostream>
#include <vector>
#include <chrono>
#include <fstream>
using namespace std;

// Returns the index of the element if found, otherwise returns -1
//int LinearSearch(vector<int> notAList, int valueToFind)
int LinearSearch(const vector<int>& notAList, int valueToFind)
{
	/*for (int i = 0; i < notAList.size(); ++i)
	{
		if (valueToFind == notAList[i])
			return i;
	}*/

	int i = 0;
	for (auto e = notAList.begin(); e != notAList.end(); ++e)
	{
		if (valueToFind == notAList[i])
			return i;
		++i;
	}

	return -1;
}

// Assumes that the input list is sorted
int BinarySearch(const vector<int>& notAList, int valueToFind)
{
	int left = 0;
	int right = notAList.size() - 1;

	int midpoint;

	while (left <= right)
	{
		midpoint = left + (right - left) / 2;

		if (valueToFind < notAList[midpoint])
			right = midpoint - 1;
		else if (valueToFind > notAList[midpoint])
			left = midpoint + 1;
		else
			return midpoint;
	}

	return -1;
}

void DoOneRun(ofstream& fout, int N)
{
	// Refactored so we can specify N, then run LinearSearch and BinarySearch for many different 
	// values of N, measuring the time they take.  Store those times in a data file, then plot them.

	/* Data file will look like (tab-separated):
	 #N	LinearSearch time (ns)	BinarySearch time (ns)
	 1	1	1
	 2	1	1
	 3	2	2
	 200	30000	10000
	*/

	// Set up a vector that contains N sequential elements
	// Searching through it should take longer the more elements we have
	vector<int> values;
	for (int i = 0; i < N; ++i)
	{
		values.push_back(i);
	}

	// Choose a value to search for out of the range
	int randomValue = rand() % N;

	// Start timing one linear search
	auto startTime = chrono::high_resolution_clock::now();
	LinearSearch(values, randomValue);
	auto stopTime = chrono::high_resolution_clock::now();

	// Calculate the total time that took, in nanoseconds
	auto nsLinear = chrono::duration_cast<chrono::nanoseconds>(stopTime - startTime).count();


	// Start timing one binary search
	startTime = chrono::high_resolution_clock::now();
	BinarySearch(values, randomValue);
	stopTime = chrono::high_resolution_clock::now();

	// Calculate the total time that took, in nanoseconds
	auto nsBinary = chrono::duration_cast<chrono::nanoseconds>(stopTime - startTime).count();

	// Output N and the two measured times to the data file
	fout << N << '\t' << nsLinear << '\t' << nsBinary << endl;
}

int main(int argc, char* argv[])
{
	cout << "Hello, algorithms!" << endl;

	// Open output file
	ofstream fout("data.txt");

	// Throw in a comment at the top of the data file
	fout << "#N\tLinearSearch time(ns)\tBinarySearch time(ns)" << endl;

	// Run the measurements for values of N going from 1 to 2000
	for (int N = 1; N < 2000; ++N)
	{
		DoOneRun(fout, N);
	}

	// Close the data file
	fout.close();

	// Then plot it
	system("gnuplot sample_plot.txt");

	cout << "Done!" << endl;
	return 0;
}