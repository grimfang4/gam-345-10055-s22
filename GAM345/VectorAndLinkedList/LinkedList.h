#pragma once

class Node
{
public:
	int value;
	Node* next;
};

// [4] -> [6] -> [1] -> [7] X
class LinkedList
{
public:
	LinkedList()
	{
		size = 0;
		head = nullptr;
	}
	~LinkedList()
	{
		// TODO: Traverse the list, delete each node as we go, then set the head pointer to nullptr.
	}
	int Size()
	{
		return size;
	}
	void PushFront(int value)
	{
		if (head == nullptr)
		{
			head = new Node();
			head->value = value;
			head->next = nullptr;
		}
		else
		{
			// [4]
			Node* newNode = new Node();
			newNode->value = value;
			newNode->next = head;
			head = newNode;
		}
	}

	int Find(int value)
	{
		int i = 0;
		for (Node* ptr = head; ptr != nullptr; ptr = ptr->next)
		{
			if (ptr->value == value)
				return i;
			++i;
		}

		return -1;
	}

private:
	Node* head;
	int size;
};