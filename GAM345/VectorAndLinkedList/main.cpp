#include <iostream>
#include "Vector.h"
#include "Random.h"
using namespace std;

int main(int argc, char* argv[])
{
	Vector myVector;

	cout << "Vector starting size (this had better be 0!): " << myVector.Size() << endl;

	//myVector.Resize(17);
	
	//myVector.size = 17;
	//cout << myVector.size << endl;

	Random rng;
	cout << "A random integer between 0 and 50: " << rng.Integer(0, 50) << endl;

	return 0;
}