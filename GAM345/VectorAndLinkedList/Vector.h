#pragma once

class Vector
{
public:
	Vector()
	{
		size = 0;

		capacity = 10;
		data = new int[capacity];
	}
	~Vector()
	{
		delete[] data;
	}
	int Size()
	{
		return size;
	}

	void Reserve(int newCapacity)
	{
		if (capacity >= newCapacity)
			return;

		int* newData = new int[newCapacity];
		// Copy the old data into the new array
		for (int i = 0; i < size; ++i)
		{
			newData[i] = data[i];
		}

		// Replace the old data pointer and stuff
		delete[] data;
		capacity = newCapacity;
		data = newData;
	}

private:
	int* data;  // internal array
	int size;  // Number of elements added
	int capacity;  // Size of the allocated array
};