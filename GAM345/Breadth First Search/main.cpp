#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <queue>
using namespace std;


class Node
{
public:
	string value;
	vector<Node*> connections;

	Node()
	{}

	Node(string name)
		: value(name)
	{}
};

class Graph
{
public:

	void Add(string nodeName)
	{
		Node* n = new Node(nodeName);
		// If insertion fails, that means this is a duplicate, so delete it.
		if (!nodes.insert(make_pair(nodeName, n)).second)
			delete n;
	}

	void AddConnection(string nodeA, string nodeB)
	{
		auto e = nodes.find(nodeA);
		if (e != nodes.end())
		{
			Node* A = e->second;

			auto f = nodes.find(nodeB);
			if (f != nodes.end())
			{
				Node* B = f->second;
				A->connections.push_back(B);
				B->connections.push_back(A);
			}
		}
	}

	bool IsConnected(string nodeA, string nodeB)
	{
		Node* A = Get(nodeA);
		if (A == nullptr)
			return false;

		queue<Node*> frontier;
		set<Node*> visited;  // For pathfinding: Keep track of the source/where you came from too.

		frontier.push(A);
		visited.insert(A);

		while (frontier.size() > 0)
		{
			Node* current = frontier.front();
			frontier.pop();

			// Here is where we do fun stuff if we weren't just checking for connections...
			if (current->value == nodeB)
				return true;

			for (auto conn : current->connections)
			{
				// Is it NOT in the visited set yet?
				if (visited.find(conn) == visited.end())
				{
					visited.insert(conn);
					frontier.push(conn);
				}
			}
		}

		return false;
	}

private:
	map<string, Node*> nodes;

	Node* Get(string name)
	{
		auto e = nodes.find(name);
		if (e != nodes.end())
			return e->second;
		return nullptr;
	}
};


int main(int argc, char* argv[])
{
	Graph g;

	g.Add("New York");
	g.Add("Hyrule");
	g.Add("San Francisco");
	g.Add("Minas Tirith");
	g.Add("Minas Morgul");
	g.Add("Australia");
	g.Add("Rome");
	g.Add("The Moon");
	g.Add("Termina");

	g.AddConnection("New York", "Hyrule");
	g.AddConnection("San Francisco", "Australia");
	g.AddConnection("San Francisco", "New York");
	g.AddConnection("Minas Tirith", "Rome");
	g.AddConnection("New York", "Rome");
	g.AddConnection("San Francisco", "Rome");
	g.AddConnection("Hyrule", "Rome");
	g.AddConnection("Hyrule", "Minas Morgul");
	g.AddConnection("The Moon", "Termina");


	cout << g.IsConnected("San Francisco", "Minas Morgul") << endl;
	cout << g.IsConnected("San Francisco", "The Moon") << endl;

	cin.get();

	return 0;
}